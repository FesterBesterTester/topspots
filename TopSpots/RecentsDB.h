//
//  RecentsDB.h
//  TopSpots
//
//  Created by rsalvo on 7/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MAX_RECENTS 20 // max number of recent photos to save
#define MAX_CACHE_SIZE 10000000

@interface RecentsDB : NSObject

@property (nonatomic) NSUInteger maxEntries;
@property (nonatomic, readonly) NSUInteger count;
-(void)add:(NSDictionary *)item withKey:(NSString *)key withDataToCache:(NSData*)cacheData;
-(NSData *)readCache:(NSString *)key;
-(NSArray*)sortedItems;
@end
