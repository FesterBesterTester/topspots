//
//  BasicAnnotation.h
//  TopSpots
//
//  Created by rsalvo on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "PhotoViewController.h" // for PhotoDataSource protocol

enum annotation_type {NONE, PHOTO, PLACE};

@interface BasicAnnotation : NSObject <MKAnnotation>
- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate withTitle:(NSString *)title withSubTitle:(NSString *)subtitle;
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;
- (void)setPhoto:(NSDictionary *)photo withDelegate:(id <PhotoDataSource>)photoDelegate;
- (void)setPlace:(NSDictionary *)place withMaxPhotos:(unsigned int)maxPhotos;
@property enum annotation_type type;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *subtitle;
@property (nonatomic, readonly, copy) NSString *title;

@property unsigned int maxPhotos;
@property (nonatomic, strong) NSDictionary *photo;
@property (nonatomic, strong) id <PhotoDataSource> photoDelegate;
@property (nonatomic, strong) NSDictionary *place;

@end
