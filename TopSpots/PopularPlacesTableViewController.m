//
//  PopularPlacesTableViewController.m
//  TopSpots
//
//  Created by rsalvo on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "PopularPlacesTableViewController.h"
#import "PlaceViewController.h"
#import "MapViewController.h"
#import "FlickrFetcher.h"
#import "BasicAnnotation.h"

#define NUM_RECENT_PHOTOS 50
#define CELL_IDENTIFIER @"Place Description"

@interface PopularPlacesTableViewController ()
@property (nonatomic) NSArray *places;
@property BOOL isVisible;
@end

@implementation PopularPlacesTableViewController

@synthesize places = _places;
@synthesize isVisible = _isVisible;


+ (BOOL)coordinatesForPlace:(NSDictionary *)place location:(CLLocationCoordinate2D*)location
{
    if (!location) return(NO);
    
    NSNumber *lat = [place objectForKey:@"latitude"];
    NSNumber *lon = [place objectForKey:@"longitude"];
    
    if (lat && lon)
    {
        location->latitude = [lat doubleValue];
        location->longitude = [lon doubleValue];
        return(YES);
    }
    return(NO);
}

+ (void)getTitleAndSubTitleFromPlace:(NSDictionary *)place title:(NSString **)title subtitle:(NSString **)subtitle
{
    NSString *placeName = [place valueForKey:@"_content"];
    NSArray *parsedPlaceName = [placeName componentsSeparatedByString:@","];

    *title = [parsedPlaceName objectAtIndex:0]; // 1st item is city
    *subtitle = @"";
    
    for (int i=1; i<parsedPlaceName.count; i++)
    {
        *subtitle = [*subtitle stringByAppendingString:[parsedPlaceName objectAtIndex:i]];
        if (i != parsedPlaceName.count - 1) *subtitle = [*subtitle stringByAppendingString:@", "];
    }
}

- (NSArray *)updatePlaces
{
    NSSortDescriptor *placeSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"_content" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:placeSortDescriptor];
    
    NSArray *places = [[FlickrFetcher topPlaces] sortedArrayUsingDescriptors:sortDescriptors];
    if (places)
    {
        return places;
    }
    else
    {
        return [[NSArray alloc] init];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        PlaceViewController *newController = segue.destinationViewController;
        
        newController.place = [self.places objectAtIndex:indexPath.row];
        newController.maxPhotos = NUM_RECENT_PHOTOS;
    }
    else if ([segue.destinationViewController isKindOfClass:[MapViewController class]])
    {
        int i;
        CLLocationCoordinate2D location;
        MapViewController *newController = segue.destinationViewController;
        MKMapView *mapview = (MKMapView*)newController.view;

        newController.segueID = @"listPlacePhotos";

        for (i=0; i<self.places.count; i++)
        {
            NSDictionary *place = [self.places objectAtIndex:i];

            if ([PopularPlacesTableViewController coordinatesForPlace:place location:&location])
            {
                NSString *title;
                NSString *subtitle;
                
                [PopularPlacesTableViewController getTitleAndSubTitleFromPlace:[self.places objectAtIndex:i]
                                                                         title:&title
                                                                      subtitle:&subtitle];
                
                BasicAnnotation *annotation = [[BasicAnnotation alloc] initWithCoordinate:location withTitle:(NSString *)title withSubTitle:(NSString *)subtitle];

                [annotation setPlace:[self.places objectAtIndex:i] withMaxPhotos:NUM_RECENT_PHOTOS];

                [mapview addAnnotation:annotation];
            }
        }
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner startAnimating];
    self.navigationItem.titleView = spinner;
    
    self.isVisible = YES;
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("places downloader", NULL);
    dispatch_async(downloadQueue, ^{        
        self.places = self.updatePlaces;

        if (self.isVisible)
        {
            if (self.places.count)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.titleView = nil; // ditch the spinner
                    self.navigationItem.title = @"Popular Places";
                    [self.tableView reloadData];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.titleView = nil; // ditch the spinner
                    self.navigationItem.title = @"Flickr Error";
                    [self.tableView reloadData];
                });
            }
        }
        else
        {
            NSLog(@"PopularPlacesTableViewController isVisible:%s", self.isVisible ? "YES" : "NO");
        }
    }); 
    dispatch_release(downloadQueue);
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.isVisible = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (YES);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return(self.places.count);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = CELL_IDENTIFIER; // reuse identifier from storyboard
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        // reuse pool is empty. allocate a new cell.
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CELL_IDENTIFIER];
    }
    // Configure the cell...
    NSString *cityName;
    NSString *subtitleName;

    [PopularPlacesTableViewController getTitleAndSubTitleFromPlace:[self.places objectAtIndex:indexPath.row] title:&cityName subtitle:&subtitleName];

    cell.textLabel.text = cityName;
    cell.detailTextLabel.text = subtitleName;

    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.    
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
