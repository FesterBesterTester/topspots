//
//  PhotoViewController.h
//  TopSpots
//
//  Created by rsalvo on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PhotoDataSource <NSObject>
@required

// This method is does filesystem and possibly network accesses, so it should
// not be called from the main thread.
- (NSData*)getPhotoData:(NSDictionary*)photo asThumbNail:(BOOL)thumbnail;

@end

@interface PhotoViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic, strong) NSDictionary* photo;
@property (nonatomic, strong) NSString *imageTitle;
@property (nonatomic, weak) id <PhotoDataSource>photoDelegate;
+ (CGRect)setInitialBounds:(CGSize)viewSize imageSize:(CGSize)imageSize;
@end
