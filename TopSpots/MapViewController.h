//
//  MapViewController.h
//  TopSpots
//
//  Created by rsalvo on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BasicAnnotation.h"

@interface MapViewController : UIViewController <MKMapViewDelegate>
@property (nonatomic, strong) NSString *segueID;
@end
