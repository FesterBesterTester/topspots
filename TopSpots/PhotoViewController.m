//
//  PhotoViewController.m
//  TopSpots
//
//  Created by rsalvo on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PhotoViewController.h"

@interface PhotoViewController ()
@property (nonatomic, strong) UIImageView *iv;
@property BOOL isVisible;
- (void)initView:(UIImage*)image animated:(BOOL)animated;
@end

@implementation PhotoViewController
@synthesize photo = _photo;
@synthesize imageTitle = _imageTitle;
@synthesize photoDelegate = _photoDelegate;
@synthesize iv = _iv;
@synthesize isVisible = _isVisible;


- (void)setPhoto:(NSDictionary *)photo
{
    BOOL needsUpdate = NO;
    
    if (_photo != photo)
    {
        needsUpdate = YES;
        _photo = photo;
    }
    
    if (needsUpdate)
    {
        [self update:YES];
    }
}

- (void)setPhotoDelegate:(id<PhotoDataSource>)delegate
{
    BOOL needsUpdate = NO;

    if ([delegate conformsToProtocol:@protocol(PhotoDataSource)])
    {
       if (_photoDelegate != delegate)
       {
           needsUpdate = YES;
           _photoDelegate = delegate;
       }
    }
    
    if (needsUpdate)
    {
        [self update:YES];
    }
}

- (id<PhotoDataSource>)photoDelegate
{
    return(_photoDelegate);
}

- (UIImageView *)iv
{
    if (!_iv) _iv = [UIImageView alloc];
    return(_iv);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)sender
{
    return(self.iv);
}

+ (CGRect)setInitialBounds:(CGSize)viewSize imageSize:(CGSize)imageSize
{
    CGFloat viewAspect = viewSize.width / viewSize.height;
    CGFloat imageAspect = imageSize.width / imageSize.height;
    CGRect bounds;

    bounds.origin.x = 0;
    bounds.origin.y = 0;
    
    if (imageAspect >= viewAspect)
    {
        bounds.size.width = imageSize.height * viewAspect;
        bounds.size.height = imageSize.height;
    }
    else
    {
        bounds.size.width = imageSize.width;
        bounds.size.height = imageSize.width / viewAspect;
    }
    return(bounds);
}

- (void)initView:(UIImage*)image animated:(BOOL)animated
{
    assert([self.view isKindOfClass:[UIScrollView class]]);
    
    UIScrollView *scrollView = (UIScrollView*)self.view;
    
    // this prevents the old view from being visible around the edges of
    // the new view if the new view is smaller than the old one.
    [self.iv removeFromSuperview];

    self.iv = [self.iv initWithImage:image];
    
    [scrollView insertSubview:self.iv atIndex:0];
    scrollView.delegate = self;
    scrollView.contentSize = scrollView.bounds.size; // frame size
    scrollView.minimumZoomScale = 0.1;
    scrollView.maximumZoomScale = 10.0;
    
    self.navigationItem.title = self.imageTitle;
    
    [scrollView zoomToRect:[PhotoViewController setInitialBounds:scrollView.bounds.size imageSize:self.iv.bounds.size] animated:animated];
}

- (void)update:(BOOL)animated
{
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner startAnimating];
    self.navigationItem.titleView = spinner;
    
    self.isVisible = YES;
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("photo downloader", NULL);
    dispatch_async(downloadQueue, ^{
        NSData *imageData = [self.photoDelegate getPhotoData:self.photo asThumbNail:NO];
        
        if (self.isVisible)
        {
            if (imageData) // if we retrieved the image OK
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.titleView = nil; // ditch the spinner
                    UIImage *image = [UIImage imageWithData:imageData];
                    [self initView:image animated:animated];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.titleView = nil; // ditch the spinner
                    self.navigationItem.title = @"Flickr Error";
                });
            }
        }
        else
        {
            NSLog(@"PhotoViewController isVisible:%s", self.isVisible ? "YES" : "NO");
        }
    }); 
    dispatch_release(downloadQueue);
}

- (void)viewWillAppear:(BOOL)animated
{
    if (self.photoDelegate)
    {
        [self update:animated];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.isVisible = NO;    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (YES);
}

@end
