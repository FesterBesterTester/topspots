//
//  BasicAnnotation.m
//  TopSpots
//
//  Created by rsalvo on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BasicAnnotation.h"



@interface BasicAnnotation ()
@end

@implementation BasicAnnotation
@synthesize type = _type;
@synthesize coordinate = _coordinate;
@synthesize maxPhotos = _maxPhotos;
@synthesize photoDelegate = _photoDelegate;
@synthesize photo = _photo;
@synthesize place = _place;
@synthesize subtitle = _subtitle;
@synthesize title = _title;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate withTitle:(NSString *)title withSubTitle:(NSString *)subtitle
{
    self = [super init];

    if (self)
    {
        _coordinate = coordinate;
        _title = title;
        _subtitle = subtitle;
    }
    return(self);
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
    _coordinate = newCoordinate;
}

- (void)setPhoto:(NSDictionary *)photo withDelegate:(id <PhotoDataSource>)photoDelegate
{
    self.type  = PHOTO;
    self.photo = photo;
    self.photoDelegate = photoDelegate;
}

- (void)setPlace:(NSDictionary *)place withMaxPhotos:(unsigned int)maxPhotos
{
    self.type = PLACE;
    self.place = place;
    self.maxPhotos = maxPhotos;
}

@end
