//
//  RecentsViewController.m
//  TopSpots
//
//  Created by rsalvo on 7/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RecentsViewController.h"
#import "MapViewController.h"
#import "BasicAnnotation.h"


#define CELL_IDENTIFIER @"Recents Photo Description"

@interface RecentsViewController ()
@end

@implementation RecentsViewController
@synthesize photos = _photos;


- (NSString *)cellID
{
    return CELL_IDENTIFIER;
}

- (NSArray *)photos
{
    if (!_photos) _photos = [self.recentsDB sortedItems];
    return _photos;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[MapViewController class]])
    {
        MapViewController *newController = segue.destinationViewController;
        newController.segueID = @"showRecentPhoto";
    }
    [super prepareForSegue:segue sender:sender];
}

- (void)viewWillAppear:(BOOL)animated
{
    _photos = [self.recentsDB sortedItems]; // refresh our photo list
    [super viewWillAppear:animated];
}

@end
