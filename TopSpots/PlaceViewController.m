//
//  PlaceViewController.m
//  TopSpots
//
//  Created by rsalvo on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlaceViewController.h"
#import "PhotoViewController.h"
#import "MapViewController.h"
#import "FlickrFetcher.h"

#define CELL_IDENTIFIER @"Place Photo Description"

@interface PlaceViewController ()
@property BOOL isVisible;
@end

@implementation PlaceViewController
@synthesize place = _place;
@synthesize isVisible = _isVisible;


- (NSString *)cellID
{
    return CELL_IDENTIFIER;
}

- (NSDictionary *)place
{
   if (!_place)
   {
       _place = [[NSDictionary alloc] init];
   }
    return _place;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[MapViewController class]])
    {
        MapViewController *newController = segue.destinationViewController;
        newController.segueID = @"showPlacePhoto";
    }
    [super prepareForSegue:segue sender:sender];
}

- (void)setPlace:(NSDictionary *)p
{
    if (!_place)
    {
        _place = [[NSDictionary alloc] init];
    }
    _place = p;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner startAnimating];
    self.navigationItem.titleView = spinner;
    
    self.isVisible = YES;

    dispatch_queue_t downloadQueue = dispatch_queue_create("place photos downloader", NULL);
    dispatch_async(downloadQueue, ^{
        self.photos = [FlickrFetcher photosInPlace:self.place maxResults:self.maxPhotos];

        if (self.isVisible)
        {
            if (self.photos.count) // places getter will initialize places from Flickr
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.titleView = nil; // ditch the spinner
                    NSString *placeString = [self.place objectForKey:@"_content"];
                    NSArray *placeDetails = [placeString componentsSeparatedByString:@", "];
                    self.navigationItem.title = [placeDetails objectAtIndex:0];
                    [self.tableView reloadData];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.titleView = nil; // ditch the spinner
                    self.navigationItem.title = @"Flickr Error";
                    [self.tableView reloadData];
                });
            }
        }
        else
        {
            NSLog(@"PlaceViewController isVisible:%s", self.isVisible ? "YES" : "NO");
        }

    }); 
    dispatch_release(downloadQueue);
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.isVisible = NO;
}

@end

