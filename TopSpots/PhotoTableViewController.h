//
//  PhotoTableViewController.h
//  TopSpots
//
//  Created by rsalvo on 7/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// --------- IMPORTANT -----------
// Derived classes must set cellID to the prototype cell's identifier
//
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PhotoViewController.h" // for PhotoDataSource
#import "RecentsDB.h"

@interface PhotoTableViewController : UITableViewController <PhotoDataSource>
@property (nonatomic) unsigned int maxPhotos;
// Pretend the propertys below this point are equivalent to C++ protected
// class members.  Objective C doesn't have a good equivalent to protected.
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) RecentsDB *recentsDB;

@end

