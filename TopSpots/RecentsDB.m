//
//  RecentsDB.m
//  TopSpots
//
//  Created by rsalvo on 7/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RecentsDB.h"

#define DEFAULTS_DB_NAME @"recentPhotos"

@interface RecentsDB ()
@property (nonatomic, strong) NSMutableDictionary *dict;
@property (nonatomic, strong) NSComparator comparator;
@end


@implementation RecentsDB
@synthesize maxEntries = _maxEntries;
@synthesize dict = _dict;
@synthesize comparator = _comparator;

-(NSUInteger)count
{
    return(self.dict.count);
}

-(void)refreshDictFromRecentsDB
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *storedRecents = [defaults objectForKey:DEFAULTS_DB_NAME];
    if (storedRecents)
    {
        _dict = [NSMutableDictionary dictionaryWithDictionary:storedRecents];
    }
    else
    {
        _dict = [[NSMutableDictionary alloc] init];
    }
}

-(NSMutableDictionary*)dict
{
    if (!_dict)
    {
        [self refreshDictFromRecentsDB];
    }
    return(_dict);
}

-(NSComparator)comparator
{
    id cmp = ^(NSDictionary *d1, NSDictionary *d2)
    {
        NSDate *time1 = [d1 valueForKey:@"timeViewed"];
        NSDate *time2 = [d2 valueForKey:@"timeViewed"];

        NSComparisonResult result = [time2 compare:time1];
        return(result);
    };
    return(cmp);
}

-(NSString *)getCacheFileName:(NSString *)key
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *appID = [[NSBundle mainBundle] bundleIdentifier];
    NSURL *cacheDirURL = [[fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *appCacheDirURL = [cacheDirURL URLByAppendingPathComponent:appID];
    NSURL *cacheFileURL = [appCacheDirURL URLByAppendingPathComponent:key];

    /*
    NSArray *contents = [fileManager contentsOfDirectoryAtURL:appCacheDirURL
                                   includingPropertiesForKeys:[[NSArray alloc] initWithObjects:NSURLNameKey, nil]
                                                      options:NSDirectoryEnumerationSkipsHiddenFiles
                                                        error:nil];
    NSLog(@"contents: %@", contents);
    */

    return ([cacheFileURL path]);
}

-(void)writeCache:(NSData *)cacheData withKey:(NSString *)key
{
    NSString *cacheFileName = [self getCacheFileName:key];
    if (cacheFileName)
    {
       [[NSFileManager defaultManager] createFileAtPath:cacheFileName contents:cacheData attributes:nil];
    }
}

-(NSData *)readCache:(NSString *)key
{
    NSData *cacheData;
    NSString *cacheFileName = [self getCacheFileName:key];
    if (cacheFileName)
    {
        cacheData = [[NSFileManager defaultManager] contentsAtPath:cacheFileName];
    }
    return(cacheData);
}


-(void)deleteCache:(NSString *)key
{
    NSString *cacheFileName = [self getCacheFileName:key];
    if (cacheFileName)
    {
        [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:cacheFileName] error:nil];
    }
}

-(void)trimCache:(int)trimAmount
{
    int removedSize = 0;
    int i;

    NSLog(@"trimCache");

    // remove cache entries to make room.  Start removing with oldest entry
    NSArray *keys = [self.dict keysSortedByValueUsingComparator:self.comparator];
        
    for (i=self.dict.count - 1; i>=0; i--)
    {
        removedSize += [self getCachedFileSize:[keys objectAtIndex:i]];
        [self deleteCache:[keys objectAtIndex:i]];
        if (removedSize >= trimAmount)
        {
            break;
        }
    }
}

-(int)getCachedFileSize:(NSString *)key
{
    int size = 0;

    NSString *cacheFileName = [self getCacheFileName:key];

    NSDictionary *fileInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:cacheFileName error:nil];

    size += [[fileInfo objectForKey:NSFileSize] intValue];
    return(size);
}

-(int)getTotalCachedFileSize
{
    NSEnumerator *enumerator = [self.dict keyEnumerator];
    int size = 0;
    id key;
    
    while ((key = [enumerator nextObject]))
    {
        size += [self getCachedFileSize:key];
    }
    return(size);
}

-(void)add:(NSDictionary *)item withKey:(NSString *)key withDataToCache:(NSData *)cacheData
{
    int cacheOverflow = self.getTotalCachedFileSize + cacheData.length - MAX_CACHE_SIZE;

    // trim cache if new entry will cause us to exceed MAX_CACHE_SIZE
    if (cacheOverflow > 0 && ![[NSFileManager defaultManager] fileExistsAtPath:[self getCacheFileName:key]])
    {
        [self trimCache:cacheOverflow];
    }
    if ((self.dict.count == self.maxEntries) && ![self.dict objectForKey:key])
    {
        // remove oldest entry
        NSArray *keys = [self.dict keysSortedByValueUsingComparator:self.comparator];
        [self.dict removeObjectForKey:[keys objectAtIndex:self.dict.count - 1]];
    }
    [self writeCache:cacheData withKey:key];
    [self.dict setObject:item forKey:key];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.dict forKey:DEFAULTS_DB_NAME];
    [defaults synchronize];
}

-(NSArray*)sortedItems
{
    [self refreshDictFromRecentsDB];
    NSArray *sortedKeys = [self.dict keysSortedByValueUsingComparator:self.comparator];
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:self.dict.count];
    
    for (int i=0; i<self.dict.count; i++)
    {
        [objects insertObject:[self.dict objectForKey:[sortedKeys objectAtIndex:i]] atIndex:i];
    }
    return [NSArray arrayWithArray:objects];
}

@end
