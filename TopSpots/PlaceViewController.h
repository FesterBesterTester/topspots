//
//  PlaceViewController.h
//  TopSpots
//
//  Created by rsalvo on 7/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoTableViewController.h"

@interface PlaceViewController : PhotoTableViewController
@property (nonatomic, strong) NSDictionary *place;
@end

