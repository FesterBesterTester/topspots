//
//  PhotoTableViewController.m
//  TopSpots
//
//  Created by rsalvo on 7/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "PhotoTableViewController.h"
#import "PhotoViewController.h"
#import "MapViewController.h"
#import "BasicAnnotation.h"
#import "FlickrFetcher.h"


@interface PhotoTableViewController ()
@property (nonatomic, strong) NSString *cellID;
@end

@implementation PhotoTableViewController
@synthesize photos = _photos;
@synthesize maxPhotos = _maxPhotos;
@synthesize recentsDB = _recentsDB;
@synthesize cellID = _cellID;


- (NSArray *)photos
{
    if (!_photos) _photos = [[NSArray alloc] init];
    return _photos;
}

- (RecentsDB *)recentsDB
{
    if (!_recentsDB) _recentsDB = [[RecentsDB alloc] init];
    _recentsDB.maxEntries = MAX_RECENTS;
    return(_recentsDB);
}

+ (BOOL)coordinatesForPhoto:(NSDictionary *)photo location:(CLLocationCoordinate2D*)location
{
    if (!location) return(NO);
    
    NSNumber *lat = [photo objectForKey:@"latitude"];
    NSNumber *lon = [photo objectForKey:@"longitude"];
    
    if (lat && lon)
    {
        location->latitude = [lat doubleValue];
        location->longitude = [lon doubleValue];
        return(YES);
    }
    return(NO);
}

+ (void)getTitleAndSubTitleFromPhoto:(NSDictionary *)photo title:(NSString **)title subtitle:(NSString **)subtitle
{
    NSString *photoTitle       = [photo valueForKey:@"title"];
    NSString *photoDescription = [photo valueForKeyPath:@"description._content"];
    
    *title = @"Unknown";
    *subtitle = @"";
    
    if (![photoTitle isEqualToString:@""] && ![photoDescription isEqualToString:@""])
    {
        *title = photoTitle;   
        *subtitle = photoDescription;
    }
    else if (![photoTitle isEqualToString:@""]) *title = photoTitle;   
    else if (![photoDescription isEqualToString:@""]) *title = photoDescription;
}

// This method is does filesystem and possibly network accesses, so it should
// not be called from the main thread.
- (NSData*)getPhotoData:(NSDictionary *)photo asThumbNail:(BOOL)thumbnail
{
    int i;
    NSString *photoID = [photo objectForKey:@"id"];
    NSData *data;
    BOOL foundPhoto = NO;

    //assert([photoID isKindOfClass:[NSNumber class]]);
    for (i=0; i<self.photos.count; i++)
    {
        if (photoID == [[self.photos objectAtIndex:i] objectForKey:@"id"])
        {
            foundPhoto = YES;
            break;
        }
    }
    if (foundPhoto)
    {
        if (thumbnail)
        {
            NSURL *imageUrl = [FlickrFetcher urlForPhoto:[self.photos objectAtIndex:i] format:FlickrPhotoFormatSquare];
            data = [NSData dataWithContentsOfURL:imageUrl];
        }
        else
        {
            data = [self.recentsDB readCache:photoID];
            if (!data) // Cache miss.  fetch from Flickr.
            {
                NSLog(@"Cache miss");
                NSURL *imageUrl = [FlickrFetcher urlForPhoto:[self.photos objectAtIndex:i] format:FlickrPhotoFormatLarge];
                data = [NSData dataWithContentsOfURL:imageUrl];
            }
            else
            {
                NSLog(@"Cache Hit!");
            }
            
            NSMutableDictionary *photo = [NSMutableDictionary dictionaryWithDictionary:[self.photos objectAtIndex:i]];
            [photo setObject:[NSDate date] forKey:@"timeViewed"];
            [self.recentsDB add:photo withKey:photoID withDataToCache:data];
        }
    }
    return(data);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[PhotoViewController class]])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        PhotoViewController *newController = segue.destinationViewController;
        
        newController.photo = [self.photos objectAtIndex:indexPath.row];
        newController.imageTitle = cell.textLabel.text;
        newController.photoDelegate = self;
    }
    else if ([segue.destinationViewController isKindOfClass:[MapViewController class]])
    {
        int i;
        CLLocationCoordinate2D location;
        MapViewController *newController = segue.destinationViewController;
        MKMapView *mapview = (MKMapView*)newController.view;
        
        for (i=0; i<self.photos.count; i++)
        {
            if ([PhotoTableViewController coordinatesForPhoto:[self.photos objectAtIndex:i] location:&location])
            {
                NSString *title;
                NSString *subtitle;
                
                [PhotoTableViewController getTitleAndSubTitleFromPhoto:[self.photos objectAtIndex:i]
                                                                 title:&title
                                                              subtitle:&subtitle];
                
                BasicAnnotation *annotation = [[BasicAnnotation alloc] initWithCoordinate:location withTitle:title withSubTitle:subtitle];

                [annotation setPhoto:[self.photos objectAtIndex:i] withDelegate:self];
                
                [mapview addAnnotation:annotation];
            }
        }
    }
    else
    {
        NSLog(@"PhotoTableViewController: unrecognized segue");
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    // notify runtime that number of cells etc may have changed.  Otherwise,
    // it won't call numberOfRowsInSection again.
    [self.view performSelector:@selector(reloadData)];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (YES);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.photos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellID];
    if (!cell)
    {
        // reuse pool is empty. allocate a new cell.
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:self.cellID];
    }
    NSString *title;
    NSString *subtitle;
    
    [PhotoTableViewController getTitleAndSubTitleFromPhoto:[self.photos objectAtIndex:indexPath.row]
                                                     title:&title
                                                  subtitle:&subtitle];
    cell.textLabel.text = title;
    cell.detailTextLabel.text = subtitle;

    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if ([self splitViewController])
    {
        PhotoViewController *detailViewController = [self.splitViewController.viewControllers objectAtIndex:1];

        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        detailViewController.photo = [self.photos objectAtIndex:indexPath.row];
        detailViewController.imageTitle = cell.textLabel.text;
        detailViewController.photoDelegate = self;
    }
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end

