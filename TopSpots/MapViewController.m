//
//  MapViewController.m
//  TopSpots
//
//  Created by rsalvo on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MKPinAnnotationView.h>
#import "MapViewController.h"
#import "PhotoViewController.h"
#import "PlaceViewController.h"
#import "FlickrFetcher.h"

@interface MapViewController ()

@end

@implementation MapViewController
@synthesize segueID = _segueID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    MKMapView *mapview = (MKMapView*)self.view;

    mapview.delegate = self;

    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated
{
    MKMapView *mapview = (MKMapView*)self.view;

    [super viewWillAppear:animated];
    
    if (mapview.annotations.count)
    {
        [mapview setRegion:self.boundingBox animated:animated];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (YES);
}

/* This algorithm only works because iOS maps cannot scroll over the Prime
   Meridian or the poles.  This means we don't need to worry about cases
   such as a negative longitude appearing to the right of a positive one.
 */
- (MKCoordinateRegion)boundingBox
{
    CLLocationDegrees latmin = 91.0, latmax = -91.0, lonmin = 181.0, lonmax = -181.0;
    MKCoordinateRegion box;
    int i;
    MKMapView *mapview = (MKMapView*)self.view;
    
    for (i=0; i<mapview.annotations.count; i++)
    {
        CLLocationCoordinate2D location = [[mapview.annotations objectAtIndex:i] coordinate];
        if (latmax < location.latitude)
            latmax = location.latitude;
        if (latmin > location.latitude)
            latmin = location.latitude;
        if (lonmax < location.longitude)
            lonmax = location.longitude;
        if (lonmin > location.longitude)
            lonmin = location.longitude;
    }

    box.span.latitudeDelta = latmax - latmin;
    box.span.longitudeDelta = lonmax - lonmin;
    box.center.latitude = latmin + (latmax - latmin) / 2;
    box.center.longitude = lonmin + (lonmax - lonmin) / 2;
    
    return(box);
}

- (MKAnnotationView *)mapView:(MKMapView *)sender
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *aView = [sender dequeueReusableAnnotationViewWithIdentifier:@"MapViewController annotation"];
    if (!aView)
    {
        aView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MapViewController annotation"];
        aView.canShowCallout = YES;
        aView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    }
    
    aView.leftCalloutAccessoryView = nil; // wipe out old image, if any

    BasicAnnotation *basicAnnotation = annotation;
    
    if (basicAnnotation.photo)
    {
        CGSize calloutSize;
        calloutSize.height = 32;
        calloutSize.width = 32;
        
        UIImageView *imageView = [[UIImageView alloc] init];
        
        CGRect bounds = imageView.bounds;
        bounds.size = calloutSize;
        imageView.bounds = bounds;
        
        aView.leftCalloutAccessoryView = imageView;
    }

    aView.annotation = annotation; // yes, this happens twice if no dequeue
    // maybe load up accessory views here (if not too expensive)?
    // or reset them and wait until mapView:didSelectAnnotationView: to load actual data
    return aView;
}


- (void)mapView:(MKMapView *)sender didSelectAnnotationView:(MKAnnotationView *)aView
{
    BasicAnnotation *annotation = aView.annotation;

    if (annotation.photo)
    {
        dispatch_queue_t downloadQueue = dispatch_queue_create("thumbnail downloader", NULL);
        dispatch_async(downloadQueue, ^{
            NSString *originalPhotoID = [annotation.photo objectForKey:@"id"];

            NSData *imageData = [annotation.photoDelegate getPhotoData:annotation.photo asThumbNail:YES];

            dispatch_async(dispatch_get_main_queue(), ^{
                if (!aView.hidden)
                {
                    BasicAnnotation *annotation = aView.annotation;
                    NSString *photoID = [annotation.photo objectForKey:@"id"];
                    
                    // check to make sure the annotation view hasn't been reused
                    // for something else while we were away
                    if ([photoID isEqualToString:originalPhotoID])
                    {
                        UIImageView *imageView = (UIImageView *)aView.leftCalloutAccessoryView;
                        imageView.image = [UIImage imageWithData:imageData];
                    }
                }
            });
        });
        dispatch_release(downloadQueue);
    }
}

- (void)updateSlaveViewController:(UIViewController*)slave fromAnnotation:(MKAnnotationView *)aView
{
    BasicAnnotation *annotation = aView.annotation;

    if ([slave isKindOfClass:[PhotoViewController class]])
    {
        PhotoViewController *newController = (PhotoViewController*)slave;
        
        newController.photo = annotation.photo;
        newController.imageTitle = annotation.title;
        newController.photoDelegate = annotation.photoDelegate;
    }
    else if ([slave isKindOfClass:[PlaceViewController class]])
    {
        PlaceViewController *newController = (PlaceViewController*)slave;
        
        newController.place = annotation.place;
        newController.maxPhotos = annotation.maxPhotos;
    }
}

- (void)mapView:(MKMapView *)sender
 annotationView:(MKAnnotationView *)aView
calloutAccessoryControlTapped:(UIControl *)control
{
    BasicAnnotation *annotation = aView.annotation;

    if ([self splitViewController] && annotation.type == PHOTO)
    {
        [self updateSlaveViewController:[self.splitViewController.viewControllers objectAtIndex:1] fromAnnotation:aView];
    }
    else
    {
        [self performSegueWithIdentifier:self.segueID sender:aView];        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[MKAnnotationView class]])
    {
        MKAnnotationView *aView = sender;
        [self updateSlaveViewController:segue.destinationViewController fromAnnotation:aView];
    }
}

@end
